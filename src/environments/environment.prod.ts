export const environment = {
  production: true,
  appBaseUrl: 'https://res-rel.xyz',
  apiBaseUrl: 'https://api.res-rel.xyz/api',
  imagesBaseUrl: 'https://images.res-rel.xyz',
};

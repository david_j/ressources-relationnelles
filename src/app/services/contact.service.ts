import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ContactMessage} from '../models/ContactMessage';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private baseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  public findAllByCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/chat/by-current-user`);
  }

  public findAllHelpMessagesByCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/chat/help/by-current-user`);
  }

  public sendContactMessageToAdmin(contactMessage: ContactMessage): Observable<any> {
    return this.http.post(`${this.baseUrl}/chat/help`, contactMessage);
  }

  public sendMessage(contactMessage: ContactMessage): Observable<any> {
    return this.http.post(`${this.baseUrl}/chat`, contactMessage);
  }

  public getChatNotifications(): Observable<any> {
    return this.http.get(`${this.baseUrl}/chat/notifications`);
  }

  public markMessagesAsRead(chatMessageList: ContactMessage[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/chat/mark-read`, chatMessageList);
  }

  // ADMIN METHODS //
  public findAll(): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/chat`);
  }

  public sendMessageFromAdmin(message: ContactMessage): Observable<any> {
    return this.http.post(`${this.baseUrl}/admin/chat`, message);
  }
}

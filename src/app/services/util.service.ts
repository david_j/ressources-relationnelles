import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Resource} from '../models/Resource.model';
import {User} from '../models/User.model';

import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  environment = environment;

  constructor(
    private http: HttpClient
  ) {
  }

  public getUserProfileImageUrl(user: User): string {
    return `${environment.imagesBaseUrl}/${user._id}/${user.profilePicture}`;
  }

  public getResourceImage(resource: Resource): string {
    return `${environment.imagesBaseUrl}/resources/${resource._id}/${resource.image}`;
  }

  public getResourceUserProfileImageUrl(resource: Resource): string {
    return `${environment.imagesBaseUrl}/${resource.user._id}/${resource.user.profilePicture}`;
  }

  public async getCurrentLocation(): Promise<any> {
    if (navigator.geolocation) {
      return new Promise((resolve, reject) =>
        navigator.geolocation.getCurrentPosition(resolve, reject)
      );
    }
  }
}

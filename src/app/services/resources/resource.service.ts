import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Resource} from '../../models/Resource.model';
import {Observable} from 'rxjs';
import {ResourceMessage} from '../../models/ResourceMessage';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  private baseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  public findAllResourcesByCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/resources/by-current-user`);
  }

  public findAllResourcesByCurrentUserRelations(): Observable<any> {
    return this.http.get(`${this.baseUrl}/resources/by-current-user-relations`);
  }

  public addResource(resource: Resource, image?: any): Observable<any> {
    const formData = new FormData();
    formData.append('resource', JSON.stringify(resource));
    if (image) {
      formData.append('image', image, image.name);
    }
    return this.http.post(`${this.baseUrl}/resources`, formData);
  }

  public updateResourceByCurrentUser(resource: Resource, image?: any): Observable<any> {
    const formData = new FormData();
    formData.append('resource', JSON.stringify(resource));
    if (image) {
      formData.append('image', image, image.name);
    }
    return this.http.put(`${this.baseUrl}/resources`, formData);
  }

  public deleteResourceByCurrentUser(resourceId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/resources/${resourceId}`);
  }

  public sendResourceMessage(comment: ResourceMessage): Observable<any> {
    return this.http.post(`${this.baseUrl}/resources/messages/send`, comment);
  }

  public likeResource(resourceId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/resources/like/${resourceId}`);
  }

  public unlikeResource(resourceId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/resources/unlike/${resourceId}`);
  }

  public answerEvent(eventId: string, answer: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/resources/events/answer/${eventId}/${answer}`);
  }

  // ADMIN METHODS
  public findAllResources(): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/resources`);
  }

  public createResource(resource: Resource): Observable<any> {
    return this.http.post(`${this.baseUrl}/admin/resources`, resource);
  }

  public updateResource(resource: Resource): Observable<any> {
    return this.http.put(`${this.baseUrl}/admin/resources`, resource);
  }

  public deleteResource(resourceId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/admin/resources/${resourceId}`);
  }

  public banResource(resourceId: string, banResource: boolean): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/resources/ban/${resourceId}/${banResource}`);
  }
}

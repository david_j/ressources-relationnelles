import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  public getCurrentUser(): Observable<User> {
    return this.http.get(`${this.baseUrl}/users/current`);
  }

  public findById(userId: string): Observable<User> {
    return this.http.get(`${this.baseUrl}/users/${userId}`);
  }

  public findByTextSearch(textSearch: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/users/search`, {textSearch});
  }

  public updateCurrentUser(user: User): Observable<User> {
    return this.http.put(`${this.baseUrl}/users/current`, user);
  }

  public uploadProfilePicture(image: any): Observable<any> {
    const formData = new FormData();
    formData.append('image', image, image.name);
    return this.http.post(`${this.baseUrl}/users/current/upload-profile-picture`, formData);
  }

  public findSettingsForCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/users/current/settings`);
  }

  public updateSettingsForCurrentUser(settings: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/users/current/settings`, settings);
  }

  // ADMIN METHODS //
  public findAllUsers(): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/users`);
  }

  public createUser(user: User): Observable<any> {
    return this.http.post(`${this.baseUrl}/admin/users`, user);
  }

  public updateUser(user: User): Observable<any> {
    return this.http.put(`${this.baseUrl}/admin/users`, user);
  }

  public deleteUser(userId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/admin/users/${userId}`);
  }

  public banUser(userId: string, banUser: boolean): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/users/ban/${userId}/${banUser}`);
  }
}

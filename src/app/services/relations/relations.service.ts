import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Relation} from '../../models/Relation.model';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RelationService {

  private baseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  public findAllRelationsForCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/relations/by-current-user`);
  }

  public findRelationsSuggestionsListByCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/relations/get-suggestions`);
  }

  public findAllRelationRequestsByCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/relation-requests/by-current-user`);
  }

  public findRelationRequestsByStatusByCurrentUser(status: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/relation-requests/${status}/by-current-user`);
  }

  public sendRelationRequest(userId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/relation-requests/send-request/${userId}`);
  }

  public answerRelationRequest(requestId: string, answer: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/relation-requests/answer/${requestId}/${answer}`);
  }

  public cancelRelationRequest(requestId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/relation-requests/cancel/${requestId}`);
  }

  public checkRequestsNotifications(): Observable<any> {
    return this.http.get(`${this.baseUrl}/relation-requests/notifications`);
  }

  public deleteRelationByRelationUser(relationUserId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/relation-requests/by-relation-user/${relationUserId}`);
  }

  public isRelationToCurrentUser(userId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/relations/is-relation-to-current-user/${userId}`);
  }

  // ADMIN METHODS
  public findAllRelations(): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/relations`);
  }

  public findAllRelationRequests(): Observable<any> {
    return this.http.get(`${this.baseUrl}/admin/relation-requests`);
  }

  public createRelation(relation: Relation): Observable<any> {
    return this.http.post(`${this.baseUrl}/admin/relations`, relation);
  }

  public updateRelation(relation: Relation): Observable<any> {
    return this.http.put(`${this.baseUrl}/admin/relations`, relation);
  }

  public deleteRelation(relationId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/admin/relations/${relationId}`);
  }

  public deleteRelationRequest(relationRequestId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/admin/relation-requests/${relationRequestId}`);
  }

}

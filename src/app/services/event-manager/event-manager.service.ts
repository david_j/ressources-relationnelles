import {Injectable} from '@angular/core';
import {Observable, Observer, Subscription} from 'rxjs';
import {filter, map, share} from 'rxjs/operators';

@Injectable()
export class EventManagerService {

  observable: Observable<| string>;
  observer: Observer<| string>;

  constructor() {
    this.observable = Observable.create((observer: Observer<| string>) => {
      this.observer = observer;
    }).pipe(share());
  }

  broadcast(event: | string): void {
    if (this.observer) {
      this.observer.next(event);
    }
  }

  subscribe(eventName: string, callback: any): Subscription {
    const subscriber: Subscription = this.observable
      .pipe(
        filter((event: any | string) => {
          if (typeof event === 'string') {
            return event === eventName;
          }
          return event.name === eventName;
        }),
        map((event: | string) => {
          if (typeof event !== 'string') {
            return event;
          }
        })
      )
      .subscribe(callback);
    return subscriber;
  }

  destroy(subscriber: Subscription): void {
    subscriber.unsubscribe();
  }

}

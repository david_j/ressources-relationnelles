import {Injectable} from '@angular/core';
import {Credentials} from '../../models/Credentials.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {EventManagerService} from '../event-manager/event-manager.service';
import {RegisterUser} from '../../models/RegisterUser.model';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  baseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient,
    private router: Router,
    private eventManager: EventManagerService
  ) {
  }

  public register(registerUser: RegisterUser): Observable<any> {
    return this.http.post(`${this.baseUrl}/register`, registerUser);
  }

  public login(credentials: Credentials): Observable<any> {
    return this.http.post(`${this.baseUrl}/login`, credentials);
  }

  public isUserGuard(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        const token = sessionStorage.getItem('token') || localStorage.getItem('token');
        if (token) {
          return this.http.get(`${this.baseUrl}/authentication/is-user`).subscribe(
            (res) => {
              this.eventManager.broadcast('loggedIn');
              return resolve(true);
            },
            (error) => {
              this.eventManager.broadcast('loggedOut');
              this.router.navigate(['/login']);
              return resolve(false);
            }
          );
        } else {
          this.eventManager.broadcast('loggedOut');
          this.router.navigate(['/login']);
          return resolve(false);
        }
      }
    );
  }

  public isAdminGuard(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        const token = sessionStorage.getItem('token') || localStorage.getItem('token');
        if (token) {
          return this.http.get(`${this.baseUrl}/authentication/is-admin`).subscribe(
            (res) => {
              this.eventManager.broadcast('loggedIn');
              return resolve(true);
            },
            (error) => {
              this.router.navigate(['/home']);
              return resolve(false);
            }
          );
        } else {
          this.router.navigate(['/home']);
          return resolve(false);
        }
      }
    );
  }

  public isUserLoggedIn(): Observable<any> {
    return this.http.get(`${this.baseUrl}/authentication/is-user`);
  }

  public isAdminLoggedIn(): Observable<any> {
    return this.http.get(`${this.baseUrl}/authentication/is-admin`);
  }

  public activateAccount(activationKey: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/activate`, {activationKey});
  }

}

export enum RequestStatus {
  PENDING,
  ACCEPTED,
  REFUSED,
}

export const RequestStatusNames = [
  'PENDING',
  'ACCEPTED',
  'REFUSED'
];

export const RequestStatusNamesFromEnum = {
  PENDING: 'PENDING',
  ACCEPTED: 'ACCEPTED',
  REFUSED: 'REFUSED'
};

export const RequestStatusValuesFromNames = {
  PENDING: 0,
  ACCEPTED: 1,
  REFUSED: 2
};

export enum Roles {
  GUEST,
  USER,
  MODERATOR,
  ADMIN,
  SUPERADMIN
}

export const RolesNames = [
  'GUEST',
  'USER',
  'MODERATOR',
  'ADMIN',
  'SUPERADMIN'
];

export const RolesNamesFromEnum = {
  GUEST: 'GUEST',
  USER: 'USER',
  MODERATOR: 'MODERATOR',
  ADMIN: 'ADMIN',
  SUPERADMIN: 'SUPERADMIN'
};

export const RolesValuesFromNames = {
  GUEST: 0,
  USER: 1,
  MODERATOR: 5,
  ADMIN: 10,
  SUPERADMIN: 100
};

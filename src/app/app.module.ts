import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {SidenavComponent} from './pages/sidenav/sidenav.component';
import {TopnavComponent} from './pages/topnav/topnav.component';
import {FooterComponent} from './pages/footer/footer.component';
import {HeaderComponent} from './pages/header/header.component';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ResourcesComponent} from './pages/resources/resources.component';
import {ResourceItemComponent} from './pages/resources/resource-item/resource-item.component';
import {ResourceService} from './services/resources/resource.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AddResourceModalComponent} from './pages/modals/add-resource-modal/add-resource-modal.component';
import {RegisterComponent} from './pages/register/register.component';
import {LoginComponent} from './pages/login/login.component';
import {HelpComponent} from './pages/help/help.component';
import {ContactComponent} from './pages/contact/contact.component';
import {RelationsComponent} from './pages/relations/relations.component';
import {SettingsComponent} from './pages/settings/settings.component';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ProfileComponent} from './pages/profile/profile.component';
import {FormsModule} from '@angular/forms';
import {RelationComponent} from './pages/relations/relation/relation.component';
import {ResourceComponent} from './pages/resources/resource/resource.component';
import {RelationItemComponent} from './pages/relations/relation-item/relation-item.component';
import {IsUserGuard} from './security/guards/is-user.guard';
import {EventManagerService} from './services/event-manager/event-manager.service';
import {AddRelationModalComponent} from './pages/modals/add-relation-modal/add-relation-modal.component';
import {XhrInterceptor} from './config/xhr-interceptor.service';
import {AdminDashboardComponent} from './pages/admin/admin-dashboard/admin-dashboard.component';
import {AdminUserListComponent} from './pages/admin/admin-user-list/admin-user-list.component';
import {AdminResourceListComponent} from './pages/admin/admin-resource-list/admin-resource-list.component';
import {AdminRelationListComponent} from './pages/admin/admin-relation-list/admin-relation-list.component';
import {UserService} from './services/user.service';
import {IsAdminGuard} from './security/guards/is-admin.guard';
import {AdminUserModalComponent} from './pages/modals/admin/admin-user-modal/admin-user-modal.component';
import {AccountActivationComponent} from './pages/account-activation/account-activation.component';
import {AdminResourceModalComponent} from './pages/modals/admin/admin-resource-modal/admin-resource-modal.component';
import {AdminRelationModalComponent} from './pages/modals/admin/admin-relation-modal/admin-relation-modal.component';
import {AdminChatComponent} from './pages/admin/admin-chat/admin-chat.component';
import {ChatComponent} from './pages/chat/chat.component';
import {SendRelationMessageComponent} from './pages/modals/send-relation-message/send-relation-message.component';
import {RelationRequestItemComponent} from './pages/relations/relation-request-item/relation-request-item.component';
import {ChatBoxComponent} from './pages/chat/chat-box/chat-box.component';
import {SendChatMessageModalComponent} from './pages/chat/send-chat-message-modal/send-chat-message-modal.component';
import {AdminRelationRequestListComponent} from './pages/admin/admin-relation-request-list/admin-relation-request-list.component';
import {AdminRelationRequestModalComponent} from './pages/modals/admin/admin-relation-request-modal/admin-relation-request-modal.component';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'activation/:activationKey',
    component: AccountActivationComponent
  },
  {
    path: 'relations',
    component: RelationsComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'relation/:id',
    component: RelationComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'resources',
    component: ResourcesComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'resource/:id',
    component: ResourceComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'help',
    component: HelpComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'chat',
    component: ContactComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'messages',
    component: ChatComponent,
    canActivate: [IsUserGuard]
  },
  {
    path: 'admin/dashboard',
    component: AdminDashboardComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: 'admin/users',
    component: AdminUserListComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: 'admin/resources',
    component: AdminResourceListComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: 'admin/relations',
    component: AdminRelationListComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: 'admin/relation-requests',
    component: AdminRelationRequestListComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: 'admin/chat',
    component: AdminChatComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'home'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidenavComponent,
    TopnavComponent,
    FooterComponent,
    HeaderComponent,
    ResourcesComponent,
    ResourceItemComponent,
    AddResourceModalComponent,
    RegisterComponent,
    LoginComponent,
    HelpComponent,
    ContactComponent,
    RelationsComponent,
    SettingsComponent,
    ProfileComponent,
    ResourceComponent,
    RelationItemComponent,
    RelationComponent,
    AddRelationModalComponent,
    AdminDashboardComponent,
    AdminUserListComponent,
    AdminResourceListComponent,
    AdminRelationListComponent,
    AdminUserModalComponent,
    AccountActivationComponent,
    AdminResourceModalComponent,
    AdminRelationModalComponent,
    AdminChatComponent,
    ChatComponent,
    SendRelationMessageComponent,
    RelationRequestItemComponent,
    ChatBoxComponent,
    SendChatMessageModalComponent,
    AdminRelationRequestListComponent,
    AdminRelationRequestModalComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    NgbModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
        timeOut: 5000,
        positionClass: 'toast-bottom-full-width',
        maxOpened: 2
      }
    ),
    FormsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true},
    HttpClient,
    ResourceService,
    ToastrService,
    EventManagerService,
    UserService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}

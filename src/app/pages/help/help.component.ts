import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  faq = [
    {
      id: 1,
      question: `Qu'est-ce qu'une ressource ?`,
      answer: `Une ressource représente ce que vous souhaitez partager avec votre entourage :
        Une sortie en famille, l'organisation d'un évènement, votre quotidien...`,
      visible: false
    },
    {
      id: 2,
      question: `Qu'est-ce qu'une relation ?`,
      answer: `Une relation est une personne avec laquelle vous avez accepté de partager des choses sur Ressources Relationnelles™.
      Elle pourra voir les ressources que vous avez publiées, vous envoyer des messages, des invitations, réagir à vos publications etc...`,
      visible: false
    },
    {
      id: 3,
      question: `Comment puis-je échanger avec mes relations ?`,
      answer: `Vous pouvez communiquer via la messagerie intégrée à Ressources Relationnelles™ !
      Vous n'avez qu'à vous rendre dans l'onglet "Messagerie" et choisir une de vos relations avec qui débuter une conversation.`,
      visible: false
    },
  ];


  constructor() {
  }

  ngOnInit(): void {
  }

  setAnswerVisibility(index: number): void {
    this.faq[index].visible = !this.faq[index].visible;
  }

}

import {Component, OnInit} from '@angular/core';
import {Resource} from '../../../../models/Resource.model';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../../../services/user.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EventManagerService} from '../../../../services/event-manager/event-manager.service';
import * as moment from 'moment';
import {ResourceService} from '../../../../services/resources/resource.service';
import {User} from '../../../../models/User.model';
import {environment} from '../../../../../environments/environment';
import {UtilService} from '../../../../services/util.service';

@Component({
  selector: 'app-admin-resource-modal',
  templateUrl: './admin-resource-modal.component.html',
  styleUrls: ['./admin-resource-modal.component.scss']
})
export class AdminResourceModalComponent implements OnInit {

  public environment = environment;

  public loaders = {
    save: false
  };

  public currentUser: User = new User();
  public resource: Resource = new Resource();
  public isViewing = false;
  public isEditing = false;
  public isCreating = false;

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private resourceService: ResourceService,
    private activeModal: NgbActiveModal,
    private eventManager: EventManagerService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
  }

  save(): void {
    if (this.isEditing) {
      this.updateResource();
    } else {
      this.createResource();
    }
  }

  public createResource(): void {
    this.loaders.save = true;
    this.resourceService.createResource(this.resource).subscribe(
      (res) => {
        this.loaders.save = false;
        this.activeModal.close();
        this.toastr.success(`La ressource a bien été créée`);
        this.eventManager.broadcast('reloadAdminResourceList');
      }, (error) => {
        console.error(error);
        this.loaders.save = false;
        this.toastr.error(`Une erreur s'est produite lors de la création de la ressource`);
      }
    );
  }

  public updateResource(): void {
    this.loaders.save = true;
    this.resourceService.updateResource(this.resource).subscribe(
      (res) => {
        this.loaders.save = false;
        this.activeModal.close();
        this.toastr.success(`La ressource a bien été mise à jour`);
        this.eventManager.broadcast('reloadAdminResourceList');
      }, (error) => {
        console.error(error);
        this.loaders.save = false;
        this.toastr.error(`Une erreur s'est produite lors de la mise à jour de la ressource`);
      }
    );
  }

  public getResourceImage(resource: Resource): string {
    return this.utilService.getResourceImage(resource);
  }

  public getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }
}

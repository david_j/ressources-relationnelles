import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminResourceModalComponent } from './admin-resource-modal.component';

describe('AdminResourceModalComponent', () => {
  let component: AdminResourceModalComponent;
  let fixture: ComponentFixture<AdminResourceModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminResourceModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminResourceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

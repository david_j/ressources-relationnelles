import {Component, OnInit} from '@angular/core';
import {User} from '../../../../models/User.model';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../../../services/user.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EventManagerService} from '../../../../services/event-manager/event-manager.service';
import * as moment from 'moment';
import {RolesNamesFromEnum, RolesValuesFromNames} from '../../../../enums/roles.enum';

@Component({
  selector: 'app-admin-user-modal',
  templateUrl: './admin-user-modal.component.html',
  styleUrls: ['./admin-user-modal.component.scss']
})
export class AdminUserModalComponent implements OnInit {

  public loaders = {
    save: false
  };

  public currentUser: User = new User();
  public user: User = new User();
  public isViewing = false;
  public isEditing = false;
  public isCreating = false;

  public RolesNamesFromEnum = RolesNamesFromEnum;
  public RolesValuesFromNames = RolesValuesFromNames;

  public roles = [
    {name: 'Invité', value: 'GUEST'},
    {name: 'Utilisateur', value: 'USER'},
    {name: 'Modérateur', value: 'MODERATOR'},
    {name: 'Administrateur', value: 'ADMIN'},
    {name: 'Super-Administrateur', value: 'SUPERADMIN'}
  ];
  public genres = [
    {name: 'Non défini', value: undefined},
    {name: 'Femme', value: 'FEMALE'},
    {name: 'Homme', value: 'MALE'},
    {name: 'Autre', value: 'OTHER'},
  ];

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private activeModal: NgbActiveModal,
    private eventManager: EventManagerService
  ) {
  }

  ngOnInit(): void {
  }

  public save(): void {
    if (this.isCreating) {
      this.createUser();
    } else {
      this.updateUser();
    }
  }

  public createUser(): void {
    this.loaders.save = true;
    this.userService.createUser(this.user).subscribe(
      (res) => {
        this.loaders.save = false;
        this.activeModal.close();
        this.toastr.success(`L'utilisateur a bien été créé`);
        this.eventManager.broadcast('reloadAdminUserList');
      }, (error) => {
        console.error(error);
        this.loaders.save = false;
        this.toastr.error(`Une erreur s'est produite lors de la création de l'utilisateur`);
      }
    );
  }

  public updateUser(): void {
    this.loaders.save = true;
    this.userService.updateUser(this.user).subscribe(
      (res) => {
        this.loaders.save = false;
        this.activeModal.close();
        this.toastr.success(`L'utilisateur a bien été mis à jour`);
        this.eventManager.broadcast('reloadAdminUserList');
      }, (error) => {
        console.error(error);
        this.loaders.save = false;
        this.toastr.error(`Une erreur s'est produite lors de la mise à jour de l'utilisateur`);
      }
    );
  }

  public getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }

}

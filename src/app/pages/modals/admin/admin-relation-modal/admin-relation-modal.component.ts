import {Component, OnInit} from '@angular/core';
import {Relation} from '../../../../models/Relation.model';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../../../services/user.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EventManagerService} from '../../../../services/event-manager/event-manager.service';
import * as moment from 'moment';
import {RelationService} from '../../../../services/relations/relations.service';
import {User} from '../../../../models/User.model';

@Component({
  selector: 'app-admin-relation-modal',
  templateUrl: './admin-relation-modal.component.html',
  styleUrls: ['./admin-relation-modal.component.scss']
})
export class AdminRelationModalComponent implements OnInit {

  public loaders = {
    save: false
  };
  public users: User[] = [];

  public currentUser: User = new User();
  public relation: Relation = new Relation();
  public isViewing = false;
  public isEditing = false;
  public isCreating = false;

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private relationService: RelationService,
    private activeModal: NgbActiveModal,
    private eventManager: EventManagerService
  ) {
  }

  ngOnInit(): void {
    this.findAllUsers();
  }

  save(): void {
    if (this.isEditing) {
      this.updateRelation();
    } else {
      this.createRelation();
    }
  }

  findAllUsers(): void {
    this.userService.findAllUsers().subscribe(
      (res) => {
        this.users = Object.assign([], res);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public createRelation(): void {
    this.loaders.save = true;
    this.relationService.createRelation(this.relation).subscribe(
      (res) => {
        this.loaders.save = false;
        this.activeModal.close();
        this.toastr.success(`La relation a bien été créée`);
        this.eventManager.broadcast('reloadAdminRelationList');
      }, (error) => {
        console.error(error);
        this.loaders.save = false;
        this.toastr.error(`Une erreur s'est produite lors de la création de la relation`);
      }
    );
  }

  public updateRelation(): void {
    this.loaders.save = true;
    this.relationService.updateRelation(this.relation).subscribe(
      (res) => {
        this.loaders.save = false;
        this.activeModal.close();
        this.toastr.success(`La relation a bien été mise à jour`);
        this.eventManager.broadcast('reloadAdminRelationList');
      }, (error) => {
        console.error(error);
        this.loaders.save = false;
        this.toastr.error(`Une erreur s'est produite lors de la mise à jour de la relation`);
      }
    );
  }

  public getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }
}

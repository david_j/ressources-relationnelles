import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRelationModalComponent } from './admin-relation-modal.component';

describe('AdminRelationModalComponent', () => {
  let component: AdminRelationModalComponent;
  let fixture: ComponentFixture<AdminRelationModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminRelationModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRelationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

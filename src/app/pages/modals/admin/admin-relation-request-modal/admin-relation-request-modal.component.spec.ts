import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRelationRequestModalComponent } from './admin-relation-request-modal.component';

describe('AdminRelationRequestModalComponent', () => {
  let component: AdminRelationRequestModalComponent;
  let fixture: ComponentFixture<AdminRelationRequestModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminRelationRequestModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRelationRequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

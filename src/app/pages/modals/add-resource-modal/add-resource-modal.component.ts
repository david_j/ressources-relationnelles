import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {Resource} from '../../../models/Resource.model';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {ResourceService} from '../../../services/resources/resource.service';
import {Router} from '@angular/router';
import {UtilService} from '../../../services/util.service';

@Component({
  selector: 'app-add-resource-modal',
  templateUrl: './add-resource-modal.component.html',
  styleUrls: ['./add-resource-modal.component.scss']
})
export class AddResourceModalComponent implements OnInit {

  resourceToAdd: Resource = new Resource();
  resourceImage: any;
  uploadedImage: any;
  uploadedImageBuffer: any;

  constructor(
    private toastr: ToastrService,
    private activeModal: NgbActiveModal,
    private http: HttpClient,
    private eventManager: EventManagerService,
    private resourceService: ResourceService,
    private router: Router,
    private util: UtilService
  ) {
  }

  ngOnInit(): void {
  }

  async onImgUploaded(event): Promise<void> {
    const files = event.target.files;
    const reader = new FileReader();
    this.uploadedImage = files[0];
    reader.readAsDataURL(this.uploadedImage);
    reader.onload = (e) => {
      this.uploadedImageBuffer = reader.result;
      // this.resourceToAdd.image = this.uploadedImageBuffer;
    };
  }

  uploadImage(event: any): void {
    this.resourceImage = event.target.files[0];
    this.onImgUploaded(event);
  }

  addResource(): void {
    this.resourceService.addResource(this.resourceToAdd, this.resourceImage).subscribe(
      (res) => {
        this.toastr.success(`La ressource a bien été créée !`);
        this.activeModal.close();
        this.router.navigate(['/resources']);
        this.eventManager.broadcast('reloadResources');
      }, (error) => {
        console.error(error);
        this.toastr.error(`Une erreur est survenue lors de la création de la ressource`);
      }
    );
  }

  removeResourceImage(): void {
    this.uploadedImage = null;
    this.uploadedImageBuffer = null;
    this.resourceImage = null;
  }

  getCurrentLocation(): void {
    let position;
    this.util.getCurrentLocation()
      .then(coordinates => {
        position = [coordinates.coords.latitude, coordinates.coords.longitude];
        this.resourceToAdd.fromPosition = JSON.stringify(position);
      })
      .catch(err => {
        console.error(err);
        return;
      });
  }

  public isFormValid(): boolean {
    return (
      (this.resourceToAdd.title && this.resourceToAdd.title.length > 0) ||
      (this.resourceToAdd.message && this.resourceToAdd.message.length > 0) ||
      (this.resourceToAdd.linkUrl && this.resourceToAdd.linkUrl.length > 0) ||
      (this.resourceImage)
    );
  }
}

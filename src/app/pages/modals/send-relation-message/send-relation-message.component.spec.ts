import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendRelationMessageComponent } from './send-relation-message.component';

describe('SendRelationMessageComponent', () => {
  let component: SendRelationMessageComponent;
  let fixture: ComponentFixture<SendRelationMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendRelationMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendRelationMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

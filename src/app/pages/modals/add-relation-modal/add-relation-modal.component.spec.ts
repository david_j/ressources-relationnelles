import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AddRelationModalComponent} from './add-relation-modal.component';

describe('AddRelationModalComponent', () => {
  let component: AddRelationModalComponent;
  let fixture: ComponentFixture<AddRelationModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddRelationModalComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRelationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

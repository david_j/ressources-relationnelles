import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {Relation} from '../../../models/Relation.model';
import {UserService} from '../../../services/user.service';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

@Component({
  selector: 'app-add-relation-modal',
  templateUrl: './add-relation-modal.component.html',
  styleUrls: ['./add-relation-modal.component.scss']
})
export class AddRelationModalComponent implements OnInit {

  public textSearch = '';

  relations: Relation[];
  relationSearch: string;

  constructor(
    private toastr: ToastrService,
    private activeModal: NgbActiveModal,
    private http: HttpClient,
    private eventManager: EventManagerService,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
  }

  findRelationsByName(): void {
    if (!isNotNullOrUndefined(this.textSearch) || this.textSearch === '') {
      return;
    }
    this.userService.findByTextSearch(this.textSearch).subscribe(
      (res) => {
        this.relations = Object.assign([], res);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  addRelation(relation): void {
    this.toastr.success(`La demande a bien été envoyée à ${relation.firstName} ${relation.lastName}`);
  }

}

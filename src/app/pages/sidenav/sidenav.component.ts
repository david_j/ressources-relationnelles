import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {RelationService} from '../../services/relations/relations.service';
import io from 'socket.io-client';
import {User} from '../../models/User.model';
import {UserService} from '../../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {ContactService} from '../../services/contact.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  public environment = environment;

  public currentUser: User;
  public notifications = {
    requests: {
      number: 0,
      docs: []
    },
    chat_messages: {
      number: 0,
      docs: []
    }
  };
  public isConnected = false;
  public isAdmin = false;
  public loaders = {
    authentication: true
  };
  private socket: any;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private eventManager: EventManagerService,
    private relationService: RelationService,
    private userService: UserService,
    private toastr: ToastrService,
    private contactService: ContactService
  ) {
    this.socket = io(environment.appBaseUrl);
  }

  ngOnInit(): void {
    this.getCurrentUser();
    this.checkIfUserIsConnected();
    this.eventManager.subscribe('loggedIn', () => {
      this.checkIfUserIsConnected();
      this.checkChatNotifications();
      this.checkRequestsNotifications();
    });
    this.eventManager.subscribe('loggedOut', () => {
      this.isConnected = false;
    });
  }

  goToPageTest(): void {
    this.router.navigate(['/home']);
  }

  private getCurrentUser(): void {
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);

        this.socket.on('chat_message__' + this.currentUser._id, data => {
          this.eventManager.broadcast('reloadChatMessages');
          this.checkChatNotifications();
          this.toastr.info(`Vous avez reçu un message !`, '', {
            positionClass: 'toast-top-right'
          });
        });

        this.checkChatNotifications();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  private checkChatNotifications(): void {
    this.contactService.getChatNotifications().subscribe(
      (res) => {
        this.notifications.chat_messages = Object.assign([], res);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  private checkIfUserIsConnected(): void {
    this.authenticationService.isUserLoggedIn().subscribe(
      (res) => {
        this.isConnected = res;
        this.checkIfUserIsAdmin();
        this.checkNotifications();
      }, (error) => {
        this.loaders.authentication = false;
        this.isConnected = false;
      }
    );
  }

  private checkNotifications(): void {
    this.checkRequestsNotifications();
  }

  private checkRequestsNotifications(): void {
    this.relationService.checkRequestsNotifications().subscribe(
      (res) => {
        this.notifications.requests = Object.assign({}, res);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  private checkIfUserIsAdmin(): void {
    this.authenticationService.isAdminLoggedIn().subscribe(
      (res) => {
        this.loaders.authentication = false;
        return this.isAdmin = res;
      }, (error) => {
        this.loaders.authentication = false;
        return this.isAdmin = false;
      }
    );
  }

}

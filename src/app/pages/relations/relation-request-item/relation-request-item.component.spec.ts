import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationRequestItemComponent } from './relation-request-item.component';

describe('RelationRequestItemComponent', () => {
  let component: RelationRequestItemComponent;
  let fixture: ComponentFixture<RelationRequestItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelationRequestItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationRequestItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

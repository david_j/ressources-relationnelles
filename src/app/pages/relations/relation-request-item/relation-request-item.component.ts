import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import * as moment from 'moment';
import {RelationService} from '../../../services/relations/relations.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {RelationRequest} from '../../../models/RelationRequest.model';
import {UtilService} from '../../../services/util.service';

@Component({
  selector: 'app-relation-request-item',
  templateUrl: './relation-request-item.component.html',
  styleUrls: ['./relation-request-item.component.scss']
})
export class RelationRequestItemComponent implements OnInit {

  @Input() relationRequest: RelationRequest;
  @Input() id: number;
  @Input() canSendSuggestion: boolean;

  environment = environment;

  constructor(
    private relationService: RelationService,
    private eventManager: EventManagerService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
  }

  cancelRequest(requestId: string): void {
    this.relationService.cancelRelationRequest(requestId).subscribe(
      (res) => {
        this.eventManager.broadcast('reloadRelations');
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public getUserProfilePicture(): string {
    return this.utilService.getUserProfileImageUrl(this.relationRequest.toUser);
  }

  public getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY');
  }

}

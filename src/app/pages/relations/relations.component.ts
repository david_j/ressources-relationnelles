import {Component, OnInit} from '@angular/core';
import {Relation} from '../../models/Relation.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AddRelationModalComponent} from '../modals/add-relation-modal/add-relation-modal.component';
import {RelationRequest} from '../../models/RelationRequest.model';
import {User} from '../../models/User.model';
import {RelationService} from '../../services/relations/relations.service';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {RequestStatus, RequestStatusNamesFromEnum} from '../../enums/relation_request_status.enum';
import {UserService} from '../../services/user.service';
import {environment} from '../../../environments/environment';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-relations',
  templateUrl: './relations.component.html',
  styleUrls: ['./relations.component.scss']
})
export class RelationsComponent implements OnInit {

  public loaders = {
    data: false
  };

  public environment = environment;

  public currentUser = new User();

  public invitationsFilter = 'received';
  public active = 1;
  // public user: User;
  public receivedRequests: RelationRequest[] = [];
  public sentRequests: RelationRequest[] = [];
  public relations: Relation[] = [];
  public relationSuggestionList: User[] = [];
  public filteredRelations: Relation[] = [];

  public RelationRequestStatusEnum = RequestStatus;
  public RequestStatusNamesFromEnum = RequestStatusNamesFromEnum;

  constructor(
    private modalService: NgbModal,
    private relationsService: RelationService,
    private eventManager: EventManagerService,
    private userService: UserService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
    this.getCurrentUser();

    this.eventManager.subscribe('reloadRelations', () => {
      this.reloadRelations();
    });
  }

  getCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.loaders.data = true;
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.reloadRelations();
      },
      (error) => {
        this.loaders.data = false;
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  reloadRelations(): void {
    this.findAllRelations();
    this.findRelationsSuggestions();
    this.findRelationRequests();
    this.filteredRelations = this.relations;
  }

  findAllRelations(): void {
    this.relationsService.findAllRelationsForCurrentUser().subscribe(
      (res) => {
        this.relations = res;
        this.filteredRelations = res;
        this.eventManager.broadcast('loadingEnded');
      }, (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  findRelationsSuggestions(): void {
    this.relationsService.findRelationsSuggestionsListByCurrentUser().subscribe(
      (res) => {
        this.relationSuggestionList = Object.assign([], res);
      }, (error) => {
        console.error(error);
      }
    );
  }

  findRelationRequests(): void {
    this.relationsService.findAllRelationRequestsByCurrentUser().subscribe(
      (res) => {
        const received = res.filter(
          doc => doc.fromUser._id !== this.currentUser._id
        );
        const sent = res.filter(
          doc => doc.fromUser._id === this.currentUser._id
        );
        this.receivedRequests = Object.assign([], received);
        this.sentRequests = Object.assign([], sent);
      }, (error) => {
        console.error(error);
      }
    );
  }

  answerRequest(requestId: string, answer: string): void {
    this.relationsService.answerRelationRequest(requestId, answer).subscribe(
      (res) => {
        this.reloadRelations();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  cancelRequest(requestId: string): void {
    this.relationsService.cancelRelationRequest(requestId).subscribe(
      (res) => {
        this.reloadRelations();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public getRelationRequestUserProfilePicture(user: User): string {
    return this.utilService.getUserProfileImageUrl(user);
  }

  openAddRelationModal(): void {
    this.modalService.open(AddRelationModalComponent);
  }

  filterRelations(event: any): void {
    const filter = event.target.value.toLowerCase();
    this.filteredRelations = this.relations.filter(
      relation => relation.relationUser.firstName.toLowerCase().includes(filter) ||
        relation.relationUser.lastName.toLowerCase().includes(filter) ||
        relation.relationUser.email.toLowerCase().includes(filter) ||
        relation.relationUser.birthDate.toLowerCase().includes(filter)
    );
  }

  filterRelationRequests(): any {
    return this.receivedRequests.filter(
      rel => this.invitationsFilter === 'received' ? rel.fromUser._id !== this.currentUser._id : rel.fromUser._id === this.currentUser._id
    );
  }

  getStatusNameFromEnum(status: string): string {
    switch (status) {
      case this.RequestStatusNamesFromEnum.ACCEPTED:
        return 'Acceptée';
      case this.RequestStatusNamesFromEnum.PENDING:
        return 'En attente';
      case this.RequestStatusNamesFromEnum.REFUSED:
        return 'Refusée';
      default:
        return 'Statut';
    }
  }

}

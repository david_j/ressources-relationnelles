import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../../models/User.model';
import {UserService} from '../../../services/user.service';
import {environment} from '../../../../environments/environment';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RelationService} from '../../../services/relations/relations.service';
import {SendChatMessageModalComponent} from '../../chat/send-chat-message-modal/send-chat-message-modal.component';
import {ToastrService} from 'ngx-toastr';

import * as moment from 'moment';
import {UtilService} from '../../../services/util.service';

@Component({
  selector: 'app-relation',
  templateUrl: './relation.component.html',
  styleUrls: ['./relation.component.scss']
})
export class RelationComponent implements OnInit {

  public isRelation = false;
  public profileIsPrivate = false;

  public loaders = {
    data: false
  };

  public user: User;
  environment = environment;
  private id: any;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private modalService: NgbModal,
    private relationService: RelationService,
    private toastr: ToastrService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.findUserById();
  }

  findUserById(): void {
    this.loaders.data = true;
    this.userService.findById(this.id).subscribe(
      (res) => {
        this.user = Object.assign({}, res);
        this.checkIfIsRelation();
      },
      (error) => {
        this.loaders.data = false;
        console.error(error);
      }
    );
  }

  openSendRelationMessageModal(): void {
    const modal = this.modalService.open(SendChatMessageModalComponent);
    modal.componentInstance.messageToSend.toUser = this.user;
  }

  deleteRelation(): void {
    this.relationService.deleteRelationByRelationUser(this.id).subscribe(
      (res) => {
        this.toastr.success(`La relation a bien été supprimée !`);
        this.checkIfIsRelation();
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur s'est produite durant la suppression de la relation`);
      }
    );
  }

  public getUserProfilePicture(): string {
    return this.utilService.getUserProfileImageUrl(this.user);
  }

  public getFormattedDate(date: string): string {
    return moment(date).locale('fr').calendar();
  }

  private checkIfIsRelation(): void {
    this.relationService.isRelationToCurrentUser(this.user._id).subscribe(
      (res) => {
        this.isRelation = res;
        this.loaders.data = false;
        this.profileIsPrivate = false;
      },
      (error) => {
        console.error(error);
        if (error.error.error === 'This profile is private') {
          this.user = null;
          this.profileIsPrivate = true;
        }
        this.loaders.data = false;
      }
    );
  }

}

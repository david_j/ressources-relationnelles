import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {environment} from '../../../../environments/environment';
import {RelationService} from '../../../services/relations/relations.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {User} from '../../../models/User.model';
import {ToastrService} from 'ngx-toastr';
import {UtilService} from '../../../services/util.service';

@Component({
  selector: 'app-relation-item',
  templateUrl: './relation-item.component.html',
  styleUrls: ['./relation-item.component.scss']
})
export class RelationItemComponent implements OnInit {

  @Input() relation: User;
  @Input() id: number;
  @Input() canSendSuggestion: boolean;

  environment = environment;

  constructor(
    private relationService: RelationService,
    private eventManager: EventManagerService,
    private toastr: ToastrService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
  }

  public sendRelationRequest(userId: string): void {
    this.relationService.sendRelationRequest(userId).subscribe(
      (res) => {
        this.eventManager.broadcast('reloadRelations');
        this.toastr.success(`Une demande a bien été envoyée à ${this.relation.firstName} ${this.relation.lastName} !`);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public getRelationUserProfilePicture(): string {
    return this.utilService.getUserProfileImageUrl(this.relation);
  }

  public getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY');
  }
}

import {Component, OnInit} from '@angular/core';
import {ContactMessage} from '../../models/ContactMessage';
import {ContactService} from '../../services/contact.service';
import {User} from '../../models/User.model';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../services/user.service';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {environment} from '../../../environments/environment';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  public environment = environment;
  public loaders = {
    data: false
  };
  public currentUser: User;
  public selectedUser: User;

  public chatMessageList: ContactMessage[] = [];
  public chatMessagesUserList: User[] = [];
  public chats: ContactMessage[] = [];
  public responseList = {};

  constructor(
    private contactService: ContactService,
    private toastr: ToastrService,
    private userService: UserService,
    private eventManager: EventManagerService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.eventManager.subscribe('reloadChatMessages', () => {
      this.findAllChatMessages();
    });
  }

  public findCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.loaders.data = true;
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.findAllChatMessages();
      },
      (error) => {
        this.loaders.data = false;
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public findAllChatMessages(): void {
    this.loaders.data = true;
    this.contactService.findAllByCurrentUser().subscribe(
      (res) => {
        this.chatMessageList = Object.assign([], res);
        this.populateChatUserList();
        this.loaders.data = false;
        setTimeout(() => {
          this.scrollToBottomForEveryMessageList();
        });
        this.eventManager.broadcast('loadingEnded');
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur est survenue lors de la récupération des messages`);
        this.loaders.data = false;
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public sendMessageToUser(user: User): void {
    const messageToSend = this.responseList[user._id];
    if (messageToSend.message.length < 1) {
      return;
    }
    this.contactService.sendMessage(messageToSend).subscribe(
      (res) => {
        this.toastr.success(`Message envoyé avec succès !`);
        this.eventManager.broadcast('reloadAdminContactMessages');
        this.responseList[user._id] = new ContactMessage();
        this.responseList[user._id].toUser = user._id;
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur s'est produite durant l'envoi du message`);
      }
    );
  }

  public getContactMessagesByUser(user: User): ContactMessage[] {
    return this.chatMessageList.filter(message => {
      if (message.fromUser._id === user._id || (message.toUser && message.toUser._id === user._id)) {
        return message;
      }
    });
  }

  public getUserProfileImage(user: User): string {
    return this.utilService.getUserProfileImageUrl(user);
  }

  public scrollToBottomForEveryMessageList(): void {
    const messageListElements = document.getElementsByClassName('message-list');
    for (const messageListElement of Object.keys(messageListElements)) {
      messageListElements[messageListElement].scrollTop = messageListElements[messageListElement].scrollHeight;
    }
  }

  public selectUserConversation(user: User): void {
    this.selectedUser = user;
    setTimeout(() => {
      document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;
    });
  }

  public getMessageListBySelectedUser(): ContactMessage[] {
    return this.chatMessageList.filter(message => message.fromUser._id === this.selectedUser._id || message.toUser._id === this.selectedUser._id);
  }

  private populateChatUserList(): void {
    for (const chatMessage of this.chatMessageList) {
      chatMessage.fromCurrentUser = this.currentUser._id === chatMessage.fromUser._id;
      if (!this.chatMessagesUserList.some(user => chatMessage.fromUser._id === user._id || chatMessage.toUser._id === user._id)) {
        if (chatMessage.fromUser._id === this.currentUser._id) {
          this.chatMessagesUserList.push(chatMessage.toUser);
          this.responseList[chatMessage.toUser._id] = new ContactMessage();
          this.responseList[chatMessage.toUser._id].toUser = chatMessage.toUser._id;
        } else {
          this.chatMessagesUserList.push(chatMessage.fromUser);
          this.responseList[chatMessage.fromUser._id] = new ContactMessage();
          this.responseList[chatMessage.fromUser._id].toUser = chatMessage.fromUser._id;
        }
      }
      this.selectedUser = this.chatMessagesUserList[0];
    }
  }
}

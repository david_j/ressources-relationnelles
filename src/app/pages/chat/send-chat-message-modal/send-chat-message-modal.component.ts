import {Component, OnInit} from '@angular/core';
import {ContactMessage} from '../../../models/ContactMessage';
import {ContactService} from '../../../services/contact.service';
import {ToastrService} from 'ngx-toastr';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-send-chat-message-modal',
  templateUrl: './send-chat-message-modal.component.html',
  styleUrls: ['./send-chat-message-modal.component.scss']
})
export class SendChatMessageModalComponent implements OnInit {

  public loaders = {
    chat: false
  };
  public messageToSend = new ContactMessage();

  constructor(
    private contactService: ContactService,
    private toastr: ToastrService,
    private eventManager: EventManagerService,
    private activeModal: NgbActiveModal
  ) {
  }

  ngOnInit(): void {
  }

  sendMessage(): void {
    this.loaders.chat = true;
    this.contactService.sendMessage(this.messageToSend).subscribe(
      (res) => {
        this.toastr.success(`Message envoyé avec succès à ${this.messageToSend.toUser.firstName} ${this.messageToSend.toUser.lastName} !`);
        this.activeModal.close();
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur est survenue lors de l'envoi du message`);
      }
    );
  }

}

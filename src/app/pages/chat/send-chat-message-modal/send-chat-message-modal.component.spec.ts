import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendChatMessageModalComponent } from './send-chat-message-modal.component';

describe('SendChatMessageModalComponent', () => {
  let component: SendChatMessageModalComponent;
  let fixture: ComponentFixture<SendChatMessageModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendChatMessageModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendChatMessageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

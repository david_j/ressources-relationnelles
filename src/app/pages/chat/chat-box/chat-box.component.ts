import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {ContactMessage} from '../../../models/ContactMessage';
import {ContactService} from '../../../services/contact.service';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../models/User.model';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';

import io from 'socket.io-client';

@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.scss']
})
export class ChatBoxComponent implements OnInit, AfterViewInit {
  @Input() chatMessageList: ContactMessage[] = [];
  @Input() currentUser: User;
  @Input() toUser: User;
  newMessage = new ContactMessage();
  private socket: any;

  constructor(
    private contactService: ContactService,
    private toastr: ToastrService,
    private eventManagerService: EventManagerService
  ) {
    this.socket = io('http://localhost:8080');
  }

  ngOnInit(): void {
    this.markMessagesAsRead();
  }

  ngAfterViewInit(): void {
    const chatBoxEl = document.getElementById('message-list');
    chatBoxEl.scrollTop = chatBoxEl.scrollHeight;
  }

  markMessagesAsRead(): void {
    this.contactService.markMessagesAsRead(this.chatMessageList).subscribe(
      (res) => {
      },
      (error) => {
        console.error(error);
      }
    );
  }

  sendMessage(): void {
    if (this.newMessage.message.length < 1) {
      return;
    }
    this.newMessage.toUser = this.toUser;
    this.newMessage.fromUser = this.currentUser;
    this.contactService.sendMessage(this.newMessage).subscribe(
      (res) => {
        // this.toastr.success(`Message envoyé avec succès !`);
        this.eventManagerService.broadcast('reloadChatMessages');
        this.socket.emit('chat_message', this.newMessage);
      }, (error) => {
        console.error(error);
        this.toastr.error(`Une erreur s'est produite lors de l'envoi du message`);
      }
    );
  }

}

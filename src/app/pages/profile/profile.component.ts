import {Component, OnInit} from '@angular/core';
import {User} from '../../models/User.model';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {UserService} from '../../services/user.service';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';
import {environment} from '../../../environments/environment';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {UtilService} from '../../services/util.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public environment = environment;

  public loaders = {
    editing: false
  };

  public user: User = new User();
  public isEditing = false;

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private toastr: ToastrService,
    private eventManager: EventManagerService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  public updateCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.loaders.editing = true;
    this.userService.updateCurrentUser(this.user).subscribe(
      (res) => {
        this.toastr.success(`Profil mis à jour avec succès !`);
        this.isEditing = false;
        this.loaders.editing = false;
        this.eventManager.broadcast('reloadUser');
        this.eventManager.broadcast('loadingEnded');
      },
      (error) => {
        console.error(error);
        this.toastr.success(`Une erreur s'est produite durant la mise à jour de votre profil`);
        this.loaders.editing = false;
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public updateUserProfilePicture(event): void {
    const image = event.target.files[0];
    if (!image) {
      return;
    }
    this.userService.uploadProfilePicture(image).subscribe(
      (res) => {
        this.getCurrentUser();
        this.eventManager.broadcast('reloadUser');
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public toggleProfileEdit(): void {
    this.isEditing = !this.isEditing;
  }

  public getUserProfilePicture(): string {
    return this.utilService.getUserProfileImageUrl(this.user);
  }

  private getCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.user = Object.assign({}, res, {birthDate: moment(res.birthDate).format('YYYY-MM-DD')});
        this.eventManager.broadcast('loadingEnded');
      },
      (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }
}

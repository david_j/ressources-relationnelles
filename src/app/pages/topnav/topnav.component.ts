import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {User} from '../../models/User.model';
import {UserService} from '../../services/user.service';
import {environment} from '../../../environments/environment';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {

  public environment = environment;

  public user: User;
  public isConnected = false;
  public isAdmin = false;

  public loaders = {
    authentication: true
  };

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private eventManager: EventManagerService,
    private userService: UserService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.checkIfUserIsConnected();
    this.eventManager.subscribe('loggedIn', () => {
      this.checkIfUserIsConnected();
      this.findCurrentUser();
    });
    this.eventManager.subscribe('loggedOut', () => {
      this.isConnected = false;
    });
    this.eventManager.subscribe('reloadUser', () => {
      this.findCurrentUser();
    });
  }

  logout(): void {
    localStorage.removeItem('token');
    sessionStorage.removeItem('token');
    this.eventManager.broadcast('loggedOut');
    this.router.navigate(['home']);
  }

  public getUserProfilePicture(): string {
    return this.utilService.getUserProfileImageUrl(this.user);
  }

  private findCurrentUser(): void {
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.user = Object.assign({}, res);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  private checkIfUserIsConnected(): void {
    this.authenticationService.isUserLoggedIn().subscribe(
      (res) => {
        this.isConnected = res;
        this.checkIfUserIsAdmin();
      }, (error) => {
        this.loaders.authentication = false;
        this.isConnected = false;
      }
    );
  }

  private checkIfUserIsAdmin(): void {
    this.authenticationService.isAdminLoggedIn().subscribe(
      (res) => {
        this.loaders.authentication = false;
        return this.isAdmin = res;
      }, (error) => {
        this.loaders.authentication = false;
        return this.isAdmin = false;
      }
    );
  }

}

import {Component, OnInit} from '@angular/core';
import {ResourceService} from '../../services/resources/resource.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AddResourceModalComponent} from '../modals/add-resource-modal/add-resource-modal.component';
import {HttpClient} from '@angular/common/http';
import {Resource} from '../../models/Resource.model';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {UserService} from '../../services/user.service';
import {User} from '../../models/User.model';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {

  user: User = new User();
  resources: Resource[] = [];

  public loaders = {
    data: false
  };

  constructor(
    private resourceService: ResourceService,
    private modalService: NgbModal,
    private http: HttpClient,
    private eventManager: EventManagerService,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    this.getCurrentUser();

    this.eventManager.subscribe('reloadResources', () => {
      this.reloadResources();
    });
  }

  getCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.loaders.data = true;
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.user = res;
        this.findAllResources();
      },
      (error) => {
        this.loaders.data = false;
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  reloadResources(): void {
    this.findAllResources();
  }


  findAllResources(): void {
    this.loaders.data = true;
    this.eventManager.broadcast('loadingStarted');
    this.resourceService.findAllResourcesByCurrentUserRelations().subscribe(
      (res) => {
        this.loaders.data = false;
        this.eventManager.broadcast('loadingEnded');
        this.resources = res;
        for (const resource1 of this.resources) {
          resource1.isFromCurrentUser = resource1.user._id === this.user._id;
          if (resource1.likes) {
            resource1.likedByCurrentUser = resource1.likes.some(like => like._id === this.user._id);
          }
          for (const message1 of resource1.messages) {
            message1.isFromCurrentUser = message1.user._id === this.user._id;
          }
          if (resource1.event) {
            resource1.event.fromCurrentUser = (resource1.event.user._id === this.user._id);
            if (resource1.event.responses) {
              resource1.event.answeredByCurrentUser = (resource1.event.responses.some(eventRes => eventRes.user._id === this.user._id));
              if (resource1.event.answeredByCurrentUser) {
                resource1.event.currentUserAnswer = (resource1.event.responses.find(eventRes => eventRes.user._id === this.user._id).response);
              }
            }
          }
        }
      }, (error) => {
        this.loaders.data = false;
        this.eventManager.broadcast('loadingEnded');
        console.error(error);
      }
    );
  }

  convertToBytes(data): any {
    let binary = '';
    const bytes = [].slice.call(new Uint8Array(data));
    bytes.forEach((b) => binary += String.fromCharCode(b));
    return binary;
  }

  openAddResourceModal(): void {
    const addResourceModal = this.modalService.open(AddResourceModalComponent);

  }

  notEmptyResourcesList(): boolean {
    return this.resources && this.resources.length > 0;
  }
}

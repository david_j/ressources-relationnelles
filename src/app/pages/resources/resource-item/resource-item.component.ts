import {Component, Input, OnInit} from '@angular/core';
import {Resource} from '../../../models/Resource.model';
import {ResourceMessage} from '../../../models/ResourceMessage';
import * as moment from 'moment';
import {ResourceService} from '../../../services/resources/resource.service';
import {ToastrService} from 'ngx-toastr';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';

import {environment} from '../../../../environments/environment';
import {User} from '../../../models/User.model';
import {UtilService} from '../../../services/util.service';

@Component({
  selector: 'app-resource-item',
  templateUrl: './resource-item.component.html',
  styleUrls: ['./resource-item.component.scss']
})
export class ResourceItemComponent implements OnInit {

  @Input() resource: Resource;
  @Input() currentUser: User;
  @Input() id: any;

  public showMessages = false;
  public resourceImage: any;
  public uploadedImage: any;
  public uploadedImageBuffer: any;

  public updatedResource: Resource;

  public isEditing = false;

  public environment = environment;
  comment: ResourceMessage = new ResourceMessage();

  constructor(
    private toastr: ToastrService,
    private resourceService: ResourceService,
    private eventManager: EventManagerService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
    this.updatedResource = Object.assign({}, this.resource);
  }

  public sendMessage(): void {
    this.comment.resource = this.resource._id;
    this.resourceService.sendResourceMessage(this.comment).subscribe(
      (res) => {
        this.toastr.success(`Le message a bien été envoyé !`);
        // this.eventManager.broadcast('reloadResources');
        this.comment.isFromCurrentUser = true;
        this.resource.messages.push(this.comment);
        this.comment = new ResourceMessage();
      }, (error) => {
        console.error(error);
        this.toastr.error(`Une erreur est survenue lors du message`);
      }
    );
  }

  public toggleEditResource(): void {
    this.isEditing = !this.isEditing;
    if (!this.isEditing) {
      this.updatedResource = Object.assign({}, this.resource);
    }
  }

  public removeResourceImage(): void {
    this.updatedResource.image = null;
  }

  uploadImage(event: any): void {
    this.resourceImage = event.target.files[0];
    this.onImgUploaded(event);
  }

  async onImgUploaded(event): Promise<void> {
    const files = event.target.files;
    const reader = new FileReader();
    this.uploadedImage = files[0];
    reader.readAsDataURL(this.uploadedImage);
    reader.onload = (e) => {
      this.uploadedImageBuffer = reader.result;
      // this.resourceToAdd.image = this.uploadedImageBuffer;
    };
  }

  public editResource(): void {
    this.resourceService.updateResourceByCurrentUser(this.updatedResource, this.resourceImage).subscribe(
      (res) => {
        this.eventManager.broadcast(('reloadResources'));
        this.isEditing = false;
        this.resource = Object.assign({}, this.updatedResource);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public deleteResource(): void {
    if (!window.confirm('Êtes-vous sûr de vouloir supprimer cette ressource ?')) {
      return;
    }
    this.resourceService.deleteResourceByCurrentUser(this.resource._id).subscribe(
      (res) => {
        this.toastr.success(`La ressource a bien été supprimée`);
        this.eventManager.broadcast('reloadResources');
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur s'est produite durant la suppression de la ressource`);
      }
    );
  }

  public likeResource(): void {
    this.resourceService.likeResource(this.resource._id).subscribe(
      (res) => {
        this.resource.likes.push(this.currentUser);
        this.resource.likedByCurrentUser = true;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public unlikeResource(): void {
    this.resourceService.unlikeResource(this.resource._id).subscribe(
      (res) => {
        const likeIndex = this.resource.likes.findIndex(like => like._id === this.currentUser._id);
        if (likeIndex > -1) {
          this.resource.likes.splice(likeIndex, 1);
        }
        this.resource.likedByCurrentUser = false;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public answerEvent(answer: string): void {
    this.resourceService.answerEvent(this.resource.event._id, answer).subscribe(
      (res) => {
        this.eventManager.broadcast('reloadResources');
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public toggleShowMessages(): void {
    this.showMessages = !this.showMessages;
  }

  public getResourceImageUrl(): string {
    return this.utilService.getResourceImage(this.resource);
  }

  public getResourceUserProfileImageUrl(): string {
    return this.utilService.getResourceUserProfileImageUrl(this.resource);
  }

  public getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY');
  }

  public getDetailedFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }

  public getFormattedResourceDate(): string {
    return moment(this.resource.createdAt).format('DD-MM-YYYY') + ' à ' + moment(this.resource.createdAt).format('hh:mm:ss');
  }
}

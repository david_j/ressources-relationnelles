import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Resource} from '../../../models/Resource.model';
import {ResourceService} from '../../../services/resources/resource.service';
import {ResourceMessage} from '../../../models/ResourceMessage';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit {

  id: any;
  resource: Resource;

  constructor(
    private route: ActivatedRoute,
    public resourceService: ResourceService
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getResourceById();
  }

  getResourceById(): void {
    return;
  }

  addComment(resource: Resource): void {
    return;
  }

}

import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ContactMessage} from '../../models/ContactMessage';
import {ContactService} from '../../services/contact.service';
import {UserService} from '../../services/user.service';
import {User} from '../../models/User.model';
import {NgbNavChangeEvent} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  active = 1;
  public currentUser: User;
  public contactMessage = new ContactMessage();
  public chatMessageList: ContactMessage[] = [];

  constructor(
    private toastr: ToastrService,
    private contactService: ContactService,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
  }

  public findCurrentUser(): void {
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = res;
        this.findAllContactMessagesByCurrentUser();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public findAllContactMessagesByCurrentUser(): void {
    this.contactService.findAllHelpMessagesByCurrentUser().subscribe(
      (res) => {
        this.chatMessageList = Object.assign([], res);
        this.chatMessageList.map(
          message => message.fromCurrentUser = message.fromUser._id === this.currentUser._id
        );
        setTimeout(() => {
          this.scrollToBottomForEveryMessageList();
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public sendContactMessage(fromChat: boolean): void {
    if (fromChat) {
      this.contactMessage.reason = null;
    }
    this.contactService.sendContactMessageToAdmin(this.contactMessage).subscribe(
      (res) => {
        this.toastr.success('Message envoyé avec succès !');
        this.contactMessage = new ContactMessage();
        this.findAllContactMessagesByCurrentUser();
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Un problème est survenu lors la tentative d'envoi du message`);
      }
    );
  }

  public scrollToBottomForEveryMessageList(): void {
    const messageListElement = document.getElementById('message-list');
    if (messageListElement) {
      messageListElement.scrollTop = messageListElement.scrollHeight;
    }
  }

  public onNavChange(changeEvent: NgbNavChangeEvent): void {
    if (changeEvent.nextId === 2) {
      setTimeout(() => {
        this.scrollToBottomForEveryMessageList();
      });
    }
  }
}

import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {RegisterUser} from '../../models/RegisterUser.model';

import * as moment from 'moment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loaders = {
    register: false
  };

  registerUser: RegisterUser = new RegisterUser();
  passwordConfirm: string;

  isVisible = {
    password: false,
    passwordConfirm: false
  };

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit(): void {
  }

  register(): void {
    if (this.registerUser.password !== this.passwordConfirm) {
      this.toastr.error('Les mots de passe ne correspondent pas');
      return;
    }

    this.loaders.register = true;
    this.authenticationService.register(this.registerUser).subscribe(
      (res) => {
        this.loaders.register = false;
        this.toastr.success('Inscription réalisée avec succès !');
        this.router.navigate(['login']);
      }, (error) => {
        this.loaders.register = false;
        this.processRegisterError(error);
        console.error(error.status);
      }
    );
  }

  elementIsFocused(element: string): boolean {
    return document.getElementById(element) === document.activeElement;
  }

  // TESTS //
  generateUser(): void {
    let randomNumber = (Math.floor(Math.random() * 100)).toString();
    if (randomNumber.length === 1) {
      randomNumber = '00' + randomNumber;
    } else if (randomNumber.length === 2) {
      randomNumber = '0' + randomNumber;
    }
    const randomYear = Math.floor(1900 + (Math.random() * 100)).toString();
    let randomMonth = (Math.floor(Math.random() * 12) + 1).toString();
    if (randomMonth.length === 1) {
      randomMonth = '0' + randomMonth;
    }
    let randomDay = (Math.floor(Math.random() * 30)).toString();
    if (randomDay.length === 1) {
      randomDay = '0' + randomDay;
    }

    const randomBirthDate = moment().format(
      randomYear + '-' + randomMonth + '-' + randomDay
    );
    this.registerUser = new RegisterUser(
      'TEST' + randomNumber,
      'Jean',
      'JeanTest' + randomNumber,
      'jean.test' + randomNumber + '@test.fr',
      '123 rue Bidon',
      'Rouen',
      '76000',
      // moment().format('1970-01-01'),
      randomBirthDate,
      'test',
    );
    this.passwordConfirm = this.registerUser.password;
    this.isVisible.password = true;
    this.isVisible.passwordConfirm = true;
    (document.getElementById('password') as HTMLInputElement).type = 'text';
    (document.getElementById('passwordConfirm') as HTMLInputElement).type = 'text';
  }

  public toggleVisibility(itemName: string): void {
    this.isVisible[itemName] = !this.isVisible[itemName];
    (document.getElementById(itemName) as HTMLInputElement).type = this.isVisible[itemName] ? 'text' : 'password';
  }

  private processRegisterError(error: any): void {
    switch (error.status) {
      case 409:
        this.toastr.error(`Le nom d'utilisateur ou l'adresse email est déjà utilisée`);
        break;
      case 400:
        this.toastr.error(`Une erreur s'est produite durant l'inscription`);
        break;
      case 500:
        this.toastr.error(`Une erreur inconnue s'est produite durant l'inscription`);
        break;
      default:
        this.toastr.error(`Une erreur inconnue s'est produite durant l'inscription`);
    }
  }

}

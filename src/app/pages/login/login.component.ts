import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {EventManagerService} from '../../services/event-manager/event-manager.service';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Credentials} from '../../models/Credentials.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public credentials: Credentials = new Credentials();
  public rememberMe = false;
  public loaders = {
    login: false
  };

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private eventManager: EventManagerService,
    private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit(): void {
  }

  login(): void {
    this.loaders.login = true;
    this.eventManager.broadcast('loadingStarted');
    this.authenticationService.login(this.credentials).subscribe(
      (res) => {
        const token = res.token;
        this.loaders.login = false;
        this.eventManager.broadcast('loadingEnded');
        this.createSession(token, this.rememberMe);
      }, (error) => {
        console.error(error);
        this.loaders.login = false;
        this.eventManager.broadcast('loadingEnded');
        this.processError(error);
      }
    );
  }

  private createSession(token: string, rememberMe: boolean): void {
    if (rememberMe) {
      localStorage.setItem('token', token);
    } else {
      sessionStorage.setItem('token', token);
    }
    this.authenticationService.isAdminLoggedIn().subscribe(
      (res) => {
        this.eventManager.broadcast('loggedIn');
        if (res) {
          this.router.navigate(['admin/users']);
        } else {
          this.router.navigate(['home']);
        }
      }, (error) => {
        this.eventManager.broadcast('loggedIn');
        this.router.navigate(['home']);
      }
    );
  }

  private processError(error): void {
    switch (error.status) {
      case 400:
        this.toastr.error(`Mot de passe incorrect`);
        break;
      case 401:
        if (error.error === 'User is not activated') {
          this.toastr.error(`L'utilisateur n'est pas activé`);
          break;
        } else if (error.error === 'User is banned') {
          this.toastr.error(`L'utilisateur a été banni`);
          break;
        } else {
          this.toastr.error(`Impossible de se connecter`);
          break;
        }
      case 404:
        if (error.error === 'User is not activated') {
          this.toastr.error(`L'utilisateur est banni`);
          break;
        } else if (error.error === 'Email does not exist') {
          this.toastr.error(`L'adresse email n'existe pas`);
          break;
        } else {
          this.toastr.error(`Impossible de se connecter`);
          break;
        }
      case 500:
        this.toastr.error(`Une erreur inconnue est survenue`);
        break;
      default:
        this.toastr.error(`Une erreur est survenue`);
        break;
    }
  }
}

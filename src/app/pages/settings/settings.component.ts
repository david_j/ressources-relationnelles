import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public loaders = {
    data: false
  };

  public settings: any;

  constructor(
    private userService: UserService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.findSettingsForCurrentUser();
  }

  public findSettingsForCurrentUser(): void {
    this.loaders.data = true;
    this.userService.findSettingsForCurrentUser().subscribe(
      (res) => {
        this.loaders.data = false;
        this.settings = Object.assign({}, res);
      },
      (error) => {
        this.loaders.data = false;
        console.error(error);
      }
    );
  }

  public updateSettings(): void {
    this.loaders.data = true;
    this.userService.updateSettingsForCurrentUser(this.settings).subscribe(
      (res) => {
        this.loaders.data = false;
        this.settings = Object.assign({}, res);
        this.toastr.success(`Vos paramètres ont bien été mis à jour`);
      },
      (error) => {
        this.loaders.data = false;
        this.toastr.success(`Une erreur s'est produite durant la mise à jour de vos paramètres`);
        console.error(error);
      }
    );
  }

}

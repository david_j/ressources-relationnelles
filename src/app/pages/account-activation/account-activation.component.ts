import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthenticationService} from '../../services/authentication/authentication.service';

@Component({
  selector: 'app-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.scss']
})
export class AccountActivationComponent implements OnInit {

  public loaders = {
    activation: false
  };

  activationKey: string;
  error = false;
  success = false;

  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    this.loaders.activation = true;
    this.route.params
      .subscribe(
        (params) => {
          this.activationKey = params.activationKey;
          this.activateAccount();
        },
        (error) => {
          this.error = true;
          this.success = false;
          this.loaders.activation = false;
          this.toastr.error(`Une erreur s'est produite durant l'activation de votre compte`);
        });
  }

  private activateAccount(): void {
    this.authenticationService.activateAccount(this.activationKey).subscribe(
      (res) => {
        this.loaders.activation = false;
        this.error = false;
        this.success = true;
        this.toastr.success(`Compte activé avec succès !`);
      },
      (error) => {
        console.error(error);
        this.loaders.activation = false;
        this.error = true;
        this.success = false;
        this.toastr.error(`Une erreur s'est produite durant l'activation de votre compte`);
      }
    );
  }

}

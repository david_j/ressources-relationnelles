import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {EventManagerService} from '../../services/event-manager/event-manager.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public isConnected = false;

  public loaders = {
    authentication: true
  };

  constructor(
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private eventManager: EventManagerService
  ) {
  }

  ngOnInit(): void {
    this.checkIfUserIsConnected();
    this.eventManager.subscribe('loggedIn', () => {
      this.checkIfUserIsConnected();
    });
    this.eventManager.subscribe('loggedOut', () => {
      this.isConnected = false;
    });
  }

  checkIfUserIsConnected(): void {
    this.authenticationService.isUserLoggedIn().subscribe(
      (res) => {
        this.loaders.authentication = false;
        this.isConnected = true;
      }, (error) => {
        this.loaders.authentication = false;
        this.isConnected = false;
      }
    );
  }

}

import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  public loaders = {
    stats: false
  };
  public stats: any[];

  constructor(
    private http: HttpClient,
    private eventManager: EventManagerService
  ) {
  }

  ngOnInit(): void {
    this.getStats();
  }

  private getStats(): void {
    this.eventManager.broadcast('loadingStarted');
    this.loaders.stats = true;
    this.http.get('/api/admin/dashboard/stats').subscribe(
      (res) => {
        this.stats = Object.assign([], res);
        this.loaders.stats = false;
        this.eventManager.broadcast('loadingEnded');
      }, (error) => {
        console.error(error);
        this.loaders.stats = false;
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

}

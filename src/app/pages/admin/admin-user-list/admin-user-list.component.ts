import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/User.model';
import {UserService} from '../../../services/user.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {Roles, RolesNames, RolesValuesFromNames} from '../../../enums/roles.enum';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AdminUserModalComponent} from '../../modals/admin/admin-user-modal/admin-user-modal.component';
import * as moment from 'moment';
import {Resource} from '../../../models/Resource.model';
import {AdminResourceModalComponent} from '../../modals/admin/admin-resource-modal/admin-resource-modal.component';

@Component({
  selector: 'app-admin-user-list',
  templateUrl: './admin-user-list.component.html',
  styleUrls: ['./admin-user-list.component.scss']
})
export class AdminUserListComponent implements OnInit {

  currentUser: User;
  userList: User[] = [];
  userRoles = Roles;
  userRolesNames = RolesNames;
  userRolesValuesFromNames = RolesValuesFromNames;

  constructor(
    private userService: UserService,
    private eventManager: EventManagerService,
    private toastr: ToastrService,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.eventManager.subscribe('reloadAdminUserList', () => {
      this.findAllUsers();
    });
  }

  public findCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.findAllUsers();
      }, (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public findAllUsers(): void {
    this.eventManager.broadcast('loadingStarted');
    this.userService.findAllUsers().subscribe(
      (res) => {
        this.eventManager.broadcast('loadingEnded');
        this.userList = Object.assign([], res);
      }, (error) => {
        this.eventManager.broadcast('loadingEnded');
        console.error(error);
      }
    );
  }

  public openUserModal(user: User, isViewing: boolean, isEditing: boolean, isCreating: boolean): void {
    const userModal = this.modalService.open(AdminUserModalComponent);
    userModal.componentInstance.currentUser = this.currentUser;
    if (isViewing || isEditing) {
      userModal.componentInstance.user = Object.assign({}, user);
    }
    userModal.componentInstance.isViewing = isViewing;
    userModal.componentInstance.isEditing = isEditing;
    userModal.componentInstance.isCreating = isCreating;
  }

  public deleteUser(user): void {
    if (!confirm('Voulez-vous vraiment supprimer cet utilisateur ?')) {
      return;
    }
    this.userService.deleteUser(user._id).subscribe(
      (res) => {
        user.isDeleted = true;
        return this.toastr.success(`L'utilisateur a bien été supprimé`);
      },
      (error) => {
        console.error(error);
        return this.toastr.error(`Une erreur s'est produite lors de la suppression de l'utilisateur`);
      }
    );
  }

  public banUser(user): void {
    this.userService.banUser(user._id, !user.isBanned).subscribe(
      (res) => {
        user.isBanned = !user.isBanned;
        return this.toastr.success(`L'utilisateur a bien été ${user.isBanned ? 'banni' : 'réactivé'}`);
      },
      (error) => {
        console.error(error);
        return this.toastr.error(`Une erreur s'est produite lors du bannissement de l'utilisateur`);
      }
    );
  }

  // GET METHODS //

  public getRoleFromRoleStr(role: string): string {
    if (role) {
      return role;
    } else {
      return '---';
    }
  }

  public getBadgeClassFromRole(role: string): string {
    if (role) {
      switch (role.toLowerCase()) {
        case 'user':
          return 'badge-dark';
        case 'moderator':
          return 'badge-primary';
        case 'admin':
          return 'badge-info';
        case 'superadmin':
          return 'badge-warning';
        default:
          return 'badge-secondary';
      }
    } else {
      return '';
    }
  }

  public hasRole(userRole: string, roles: string[]): boolean {
    if (userRole && roles) {
      return roles.includes(userRole);
    } else {
      return false;
    }
  }

  public canDeleteUser(user: User): boolean {
    return !user.isDeleted && this.userRolesValuesFromNames[user.role] < this.userRolesValuesFromNames[this.currentUser.role];
  }

  public canBanUser(user: User): boolean {
    return !user.isDeleted && this.userRolesValuesFromNames[user.role] < this.userRolesValuesFromNames[this.currentUser.role];
  }

}

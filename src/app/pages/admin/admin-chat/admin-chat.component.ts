import {Component, OnInit} from '@angular/core';
import {ContactService} from '../../../services/contact.service';
import {ContactMessage} from '../../../models/ContactMessage';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../models/User.model';
import {UserService} from '../../../services/user.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';

@Component({
  selector: 'app-admin-chat',
  templateUrl: './admin-chat.component.html',
  styleUrls: ['./admin-chat.component.scss']
})
export class AdminChatComponent implements OnInit {

  public loaders = {
    messages: false
  };

  public currentUser: User;

  public contactMessageList: ContactMessage[] = [];
  public contactMessagesUserList: User[] = [];
  public responseList = {};

  constructor(
    private contactService: ContactService,
    private toastr: ToastrService,
    private userService: UserService,
    private eventManager: EventManagerService
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.eventManager.subscribe('reloadAdminContactMessages', () => {
      this.findAllContactMessages();
    });
  }

  public findCurrentUser(): void {
    this.loaders.messages = true;
    this.eventManager.broadcast('loadingStarted');
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.findAllContactMessages();
      },
      (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
        this.loaders.messages = false;
      }
    );
  }

  public findAllContactMessages(): void {
    this.loaders.messages = true;
    this.contactService.findAll().subscribe(
      (res) => {
        this.contactMessageList = Object.assign([], res);
        for (const contactMessage of this.contactMessageList) {
          contactMessage.fromCurrentUser = this.currentUser._id === contactMessage.fromUser._id;
          if (!this.contactMessagesUserList.some(user => contactMessage.fromUser._id === user._id) && contactMessage.fromUser.role === 'USER') {
            this.contactMessagesUserList.push(contactMessage.fromUser);
            this.responseList[contactMessage.fromUser._id] = new ContactMessage();
            this.responseList[contactMessage.fromUser._id].toUser = contactMessage.fromUser._id;
          }
        }
        this.loaders.messages = false;
        setTimeout(() => {
          this.scrollToBottomForEveryMessageList();
        });
        this.eventManager.broadcast('loadingEnded');
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur est survenue lors de la récupération des messages`);
        this.loaders.messages = false;
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public sendMessageToUser(user: User): void {
    const messageToSend = this.responseList[user._id];
    if (messageToSend.message.length < 1) {
      return;
    }
    this.contactService.sendMessageFromAdmin(messageToSend).subscribe(
      (res) => {
        this.toastr.success(`Message envoyé avec succès !`);
        this.eventManager.broadcast('reloadAdminContactMessages');
        this.responseList[user._id] = new ContactMessage();
        this.responseList[user._id].toUser = user._id;
      },
      (error) => {
        console.error(error);
        this.toastr.error(`Une erreur s'est produite durant l'envoi du message`);
      }
    );
  }

  public getContactMessagesByUser(user: User): ContactMessage[] {
    return this.contactMessageList.filter(message => {
      if (message.fromUser._id === user._id || (message.toUser && message.toUser._id === user._id)) {
        return message;
      }
    });
  }

  public scrollToBottomForEveryMessageList(): void {
    const messageListElements = document.getElementsByClassName('message-list');
    for (const messageListElement of Object.keys(messageListElements)) {
      messageListElements[messageListElement].scrollTop = messageListElements[messageListElement].scrollHeight;
    }
  }
}

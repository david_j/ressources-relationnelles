import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminResourceListComponent} from './admin-resource-list.component';

describe('AdminResourceListComponent', () => {
  let component: AdminResourceListComponent;
  let fixture: ComponentFixture<AdminResourceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminResourceListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminResourceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

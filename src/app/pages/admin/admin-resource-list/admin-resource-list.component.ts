import {Component, OnInit} from '@angular/core';
import {Resource} from '../../../models/Resource.model';
import {UserService} from '../../../services/user.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ResourceService} from '../../../services/resources/resource.service';
import * as moment from 'moment';
import {User} from '../../../models/User.model';
import {AdminResourceModalComponent} from '../../modals/admin/admin-resource-modal/admin-resource-modal.component';
import {RolesValuesFromNames} from '../../../enums/roles.enum';

@Component({
  selector: 'app-admin-resource-list',
  templateUrl: './admin-resource-list.component.html',
  styleUrls: ['./admin-resource-list.component.scss']
})
export class AdminResourceListComponent implements OnInit {

  currentUser: User;
  resourceList: Resource[] = [];
  userRolesValuesFromNames = RolesValuesFromNames;

  constructor(
    private userService: UserService,
    private resourceService: ResourceService,
    private eventManager: EventManagerService,
    private toastr: ToastrService,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.eventManager.subscribe('reloadAdminResourceList', () => {
      this.findAllResources();
    });
  }

  public findCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.findAllResources();
      }, (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public findAllResources(): void {
    this.eventManager.broadcast('loadingStarted');
    this.resourceService.findAllResources().subscribe(
      (res) => {
        this.eventManager.broadcast('loadingEnded');
        this.resourceList = Object.assign([], res);
      }, (error) => {
        this.eventManager.broadcast('loadingEnded');
        console.error(error);
      }
    );
  }

  public openResourceModal(resource: Resource, isViewing: boolean, isEditing: boolean, isCreating: boolean): void {
    const resourceModal = this.modalService.open(AdminResourceModalComponent);
    resourceModal.componentInstance.user = this.currentUser;
    if (isViewing || isEditing) {
      resourceModal.componentInstance.resource = Object.assign({}, resource);
    }
    resourceModal.componentInstance.isViewing = isViewing;
    resourceModal.componentInstance.isEditing = isEditing;
    resourceModal.componentInstance.isCreating = isCreating;
  }

  public deleteResource(resource): void {
    if (!confirm('Voulez-vous vraiment supprimer cette ressource ?')) {
      return;
    }
    this.resourceService.deleteResource(resource._id).subscribe(
      (res) => {
        resource = res;
        return this.toastr.success(`La ressource a bien été supprimée`);
      },
      (error) => {
        console.error(error);
        return this.toastr.error(`Une erreur s'est produite lors de la suppression de la ressource`);
      }
    );
  }

  public banResource(resource): void {
    this.resourceService.banResource(resource._id, !resource.isBanned).subscribe(
      (res) => {
        resource.isBanned = !resource.isBanned;
        return this.toastr.success(`La ressource a bien été ${resource.isBanned ? 'bannie' : 'réactivée'}`);
      },
      (error) => {
        console.error(error);
        return this.toastr.error(`Une erreur s'est produite lors du bannissement de la ressource`);
      }
    );
  }


  // GET METHODS
  getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }

  public canDeleteResource(resource: Resource): boolean {
    return !resource.isDeleted && this.userRolesValuesFromNames[resource.user.role] < this.userRolesValuesFromNames[this.currentUser.role];
  }

  public canBanResource(resource: Resource): boolean {
    return !resource.isDeleted && this.userRolesValuesFromNames[resource.user.role] < this.userRolesValuesFromNames[this.currentUser.role];
  }

}

import {Component, OnInit} from '@angular/core';
import {Relation} from '../../../models/Relation.model';
import {UserService} from '../../../services/user.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RelationService} from '../../../services/relations/relations.service';
import * as moment from 'moment';
import {User} from '../../../models/User.model';
import {RolesValuesFromNames} from '../../../enums/roles.enum';
import {RelationRequest} from '../../../models/RelationRequest.model';
import {AdminRelationRequestModalComponent} from '../../modals/admin/admin-relation-request-modal/admin-relation-request-modal.component';

@Component({
  selector: 'app-admin-relation-request-list',
  templateUrl: './admin-relation-request-list.component.html',
  styleUrls: ['./admin-relation-request-list.component.scss']
})
export class AdminRelationRequestListComponent implements OnInit {

  currentUser: User;
  relationRequestList: RelationRequest[] = [];
  userRolesValuesFromNames = RolesValuesFromNames;

  constructor(
    private userService: UserService,
    private relationService: RelationService,
    private eventManager: EventManagerService,
    private toastr: ToastrService,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.eventManager.subscribe('reloadAdminRelationRequestList', () => {
      this.findAllRelationRequests();
    });
  }

  public findCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.findAllRelationRequests();
      }, (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public findAllRelationRequests(): void {
    this.eventManager.broadcast('loadingStarted');
    this.relationService.findAllRelationRequests().subscribe(
      (res) => {
        this.eventManager.broadcast('loadingEnded');
        this.relationRequestList = Object.assign([], res);
        console.log(res);
      }, (error) => {
        this.eventManager.broadcast('loadingEnded');
        console.error(error);
      }
    );
  }

  public openRelationRequestModal(relationRequest: Relation, isViewing: boolean, isEditing: boolean, isCreating: boolean): void {
    const relationModal = this.modalService.open(AdminRelationRequestModalComponent);
    relationModal.componentInstance.user = this.currentUser;
    if (isViewing || isEditing) {
      relationModal.componentInstance.relationRequest = Object.assign({}, relationRequest);
    }
    relationModal.componentInstance.isViewing = isViewing;
    relationModal.componentInstance.isEditing = isEditing;
    relationModal.componentInstance.isCreating = isCreating;
  }

  public deleteRelationRequest(relationRequest): void {
    if (!confirm('Voulez-vous vraiment supprimer cette requête de relation ?')) {
      return;
    }
    this.relationService.deleteRelationRequest(relationRequest._id).subscribe(
      (res) => {
        relationRequest = res;
        this.findAllRelationRequests();
        return this.toastr.success(`La requête de relation a bien été supprimée`);
      },
      (error) => {
        console.error(error);
        return this.toastr.error(`Une erreur s'est produite lors de la suppression de la requête de relation`);
      }
    );
  }

  // GET METHODS
  getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }

  public canDeleteRelationRequest(relationRequest: RelationRequest): boolean {
    if (!relationRequest.fromUser) {
      return false;
    }
    return !relationRequest.isDeleted &&
      this.userRolesValuesFromNames[relationRequest.fromUser.role] < this.userRolesValuesFromNames[this.currentUser.role];
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRelationRequestListComponent } from './admin-relation-request-list.component';

describe('AdminRelationRequestListComponent', () => {
  let component: AdminRelationRequestListComponent;
  let fixture: ComponentFixture<AdminRelationRequestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminRelationRequestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRelationRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {Relation} from '../../../models/Relation.model';
import {UserService} from '../../../services/user.service';
import {EventManagerService} from '../../../services/event-manager/event-manager.service';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RelationService} from '../../../services/relations/relations.service';
import * as moment from 'moment';
import {User} from '../../../models/User.model';
import {AdminRelationModalComponent} from '../../modals/admin/admin-relation-modal/admin-relation-modal.component';
import {RolesValuesFromNames} from '../../../enums/roles.enum';

@Component({
  selector: 'app-admin-relation-list',
  templateUrl: './admin-relation-list.component.html',
  styleUrls: ['./admin-relation-list.component.scss']
})
export class AdminRelationListComponent implements OnInit {

  currentUser: User;
  relationList: Relation[] = [];
  userRolesValuesFromNames = RolesValuesFromNames;

  constructor(
    private userService: UserService,
    private relationService: RelationService,
    private eventManager: EventManagerService,
    private toastr: ToastrService,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.findCurrentUser();
    this.eventManager.subscribe('reloadAdminRelationList', () => {
      this.findAllRelations();
    });
  }

  public findCurrentUser(): void {
    this.eventManager.broadcast('loadingStarted');
    this.userService.getCurrentUser().subscribe(
      (res) => {
        this.currentUser = Object.assign({}, res);
        this.findAllRelations();
      }, (error) => {
        console.error(error);
        this.eventManager.broadcast('loadingEnded');
      }
    );
  }

  public findAllRelations(): void {
    this.eventManager.broadcast('loadingStarted');
    this.relationService.findAllRelations().subscribe(
      (res) => {
        this.eventManager.broadcast('loadingEnded');
        this.relationList = Object.assign([], res);
      }, (error) => {
        this.eventManager.broadcast('loadingEnded');
        console.error(error);
      }
    );
  }

  public openRelationModal(relation: Relation, isViewing: boolean, isEditing: boolean, isCreating: boolean): void {
    const relationModal = this.modalService.open(AdminRelationModalComponent);
    relationModal.componentInstance.user = this.currentUser;
    if (isViewing || isEditing) {
      relationModal.componentInstance.relation = Object.assign({}, relation);
    }
    relationModal.componentInstance.isViewing = isViewing;
    relationModal.componentInstance.isEditing = isEditing;
    relationModal.componentInstance.isCreating = isCreating;
  }

  public deleteRelation(relation): void {
    if (!confirm('Voulez-vous vraiment supprimer cette relation ?')) {
      return;
    }
    this.relationService.deleteRelation(relation._id).subscribe(
      (res) => {
        relation = res;
        this.findAllRelations();
        return this.toastr.success(`La relation a bien été supprimée`);
      },
      (error) => {
        console.error(error);
        return this.toastr.error(`Une erreur s'est produite lors de la suppression de la relation`);
      }
    );
  }

  // GET METHODS
  getFormattedDate(date: string): string {
    return moment(date).format('DD-MM-YYYY') + ' à ' + moment(date).format('hh:mm:ss');
  }

  public canDeleteRelation(relation: Relation): boolean {
    if (!relation.user) {
      return false;
    }
    return !relation.isDeleted && this.userRolesValuesFromNames[relation.user.role] < this.userRolesValuesFromNames[this.currentUser.role];
  }
}

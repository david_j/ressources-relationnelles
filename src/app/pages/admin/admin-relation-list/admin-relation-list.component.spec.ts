import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminRelationListComponent} from './admin-relation-list.component';

describe('AdminRelationListComponent', () => {
  let component: AdminRelationListComponent;
  let fixture: ComponentFixture<AdminRelationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminRelationListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRelationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

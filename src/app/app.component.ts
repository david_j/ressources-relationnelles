import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {AuthenticationService} from './services/authentication/authentication.service';
import {EventManagerService} from './services/event-manager/event-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'ressources-relationnelles';

  public isConnected: any = false;

  public loaders = {
    global: false,
    authentication: true
  };


  constructor(
    private location: Location,
    private authenticationService: AuthenticationService,
    private eventManager: EventManagerService
  ) {
  }

  ngOnInit(): void {
    this.checkIfUserIsConnected();
    this.eventManager.subscribe('loggedInOrOut', () => {
      this.checkIfUserIsConnected();
    });
    this.eventManager.subscribe('loadingStarted', () => {
      this.loaders.global = true;
    });
    this.eventManager.subscribe('loadingEnded', () => {
      this.loaders.global = false;
    });
  }

  checkIfUserIsConnected(): void {
    // this.isConnected = this.authenticationService.isLoggedIn();
    this.loaders.authentication = false;
  }

  goBack(): void {
    this.location.back();
  }

  goForward(): void {
    this.location.forward();
  }

}

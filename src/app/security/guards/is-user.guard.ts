import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../../services/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class IsUserGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService
  ) {
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.authenticationService.isUserGuard();
  }

}

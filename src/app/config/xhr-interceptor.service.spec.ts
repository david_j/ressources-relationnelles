import {TestBed} from '@angular/core/testing';

import {XhrInterceptor} from './xhr-interceptor.service';

describe('XhrInterceptorService', () => {
  let service: XhrInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(XhrInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

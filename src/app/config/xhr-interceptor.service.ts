import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    let authToken = sessionStorage.getItem('token') || localStorage.getItem('token');
    if (!authToken) {
      authToken = '';
    }
    const xhr = req.clone({
      headers: req.headers
        .set('X-Requested-With', 'XMLHttpRequest')
        .set('Authorization', authToken)
    });
    return next.handle(xhr);
  }
}

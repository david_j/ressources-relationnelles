import {RelationRequest} from './RelationRequest.model';
import {User} from './User.model';

export class Relation {
  _id?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  birthDate?: string;
  profilePicture?: string;
  user?: User;
  relationUser?: User;
  relationRequests?: RelationRequest[] = [];
  createdAt?: string;
  isEdited?: boolean;
  editedAt?: string;
  isDeleted?: boolean;
  deletedAt?: string;


  constructor(id?: string, email?: string, firstName?: string, lastName?: string, birthDate?: string, profilePicture?: string, user?: User, relationUser?: User, relationRequests?: RelationRequest[], createdAt?: string, isEdited?: boolean, editedAt?: string, isDeleted?: boolean, deletedAt?: string) {
    this._id = id;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.profilePicture = profilePicture;
    this.user = user;
    this.relationUser = relationUser;
    this.relationRequests = relationRequests;
    this.createdAt = createdAt;
    this.isEdited = isEdited;
    this.editedAt = editedAt;
    this.isDeleted = isDeleted;
    this.deletedAt = deletedAt;
  }
}

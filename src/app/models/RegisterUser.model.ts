export class RegisterUser {
  lastName?: string;
  firstName?: string;
  username?: string;
  email?: string;
  address?: string;
  city?: string;
  postalCode?: string;
  birthDate?: string;
  password?: string;

  constructor(lastName?: string, firstName?: string, username?: string, email?: string, address?: string, city?: string, postalCode?: string, birthDate?: string, password?: string) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.username = username;
    this.email = email;
    this.address = address;
    this.city = city;
    this.postalCode = postalCode;
    this.birthDate = birthDate;
    this.password = password;
  }
}

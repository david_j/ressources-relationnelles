import {ResourceMessage} from './ResourceMessage';
import {User} from './User.model';
import {ResourceEvent} from './ResourceEvent';

export class Resource {
  _id?: string;
  user?: User;
  event?: ResourceEvent;
  type = 'default';
  title?: string;
  message?: string;
  imageUrl?: string;
  image?: any;
  linkUrl?: string;
  isBanned?: boolean;
  createdAt?: string;
  editedAt?: string;
  deletedAt?: string;
  isEdited?: boolean;
  isDeleted?: boolean;
  messages?: ResourceMessage[];
  likes?: User[];
  fromPosition?: string;
  // Not stored in database //
  isFromCurrentUser?: boolean;
  likedByCurrentUser?: boolean;
}

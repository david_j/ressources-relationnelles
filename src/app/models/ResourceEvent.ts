import {User} from './User.model';

export class ResourceEvent {
  _id?: string;
  user?: User;
  title?: string;
  message?: string;
  startDate?: string;
  endDate?: string;
  responses?: any[];
  // Not stored in databse
  fromCurrentUser?: boolean;
  answeredByCurrentUser?: boolean;
  currentUserAnswer: string;


  constructor(id?: string, user?: User, title?: string, message?: string, startDate?: string, endDate?: string, responses?: any[], fromCurrentUser?: boolean, answeredByCurrentUser?: boolean, currentUserAnswer?: string) {
    this._id = id;
    this.user = user;
    this.title = title;
    this.message = message;
    this.startDate = startDate;
    this.endDate = endDate;
    this.responses = responses;
    this.fromCurrentUser = fromCurrentUser;
    this.answeredByCurrentUser = answeredByCurrentUser;
    this.currentUserAnswer = currentUserAnswer;
  }
}

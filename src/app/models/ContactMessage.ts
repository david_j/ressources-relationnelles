import {User} from './User.model';

export class ContactMessage {
  _id?: string;
  fromUser?: User;
  toUser?: User;
  responseToMessage?: ContactMessage;
  reason?: string;
  message?: string;
  isResponse?: boolean;
  fromHelpChat?: boolean;
  isRead?: boolean;
  createdAt?: string;
  editedAt?: string;
  deletedAt?: string;
  isEdited?: boolean;
  isDeleted?: boolean;
  responseMessage?: ContactMessage;
  fromCurrentUser?: boolean;

  constructor(id?: string, fromUser?: User, toUser?: User, responseToMessage?: ContactMessage, reason?: string, message?: string, isResponse?: boolean, createdAt?: string, editedAt?: string, deletedAt?: string, isEdited?: boolean, isDeleted?: boolean, responseMessage?: ContactMessage, fromCurrentUser?: boolean) {
    this._id = id;
    this.fromUser = fromUser;
    this.toUser = toUser;
    this.responseToMessage = responseToMessage;
    this.reason = reason;
    this.message = message;
    this.isResponse = isResponse;
    this.createdAt = createdAt;
    this.editedAt = editedAt;
    this.deletedAt = deletedAt;
    this.isEdited = isEdited;
    this.isDeleted = isDeleted;
    this.responseMessage = responseMessage;
    this.fromCurrentUser = fromCurrentUser;
  }
}

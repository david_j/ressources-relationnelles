export class User {
  _id?: string;
  email?: string;
  username?: string;
  firstName?: string;
  lastName?: string;
  birthDate?: string;
  phone?: string;
  genre?: string;
  address?: string;
  city?: string;
  postalCode?: string;
  profilePicture?: any;
  role?: string;
  isBanned?: string;
  createdAt?: string;
  editedAt?: string;
  deletedAt?: string;
  isEdited?: boolean;
  isDeleted?: boolean;
  userSettings?: any;


  constructor(_id?: string, email?: string, username?: string, firstName?: string, lastName?: string, birthDate?: string, phone?: string, genre?: string, address?: string, city?: string, postalCode?: string, role?: string, isBanned?: string, createdAt?: string, editedAt?: string, deletedAt?: string, isEdited?: boolean, isDeleted?: boolean, userSettings?: any) {
    this._id = _id;
    this.email = email;
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.phone = phone;
    this.genre = genre;
    this.address = address;
    this.city = city;
    this.postalCode = postalCode;
    this.role = role;
    this.isBanned = isBanned;
    this.createdAt = createdAt;
    this.editedAt = editedAt;
    this.deletedAt = deletedAt;
    this.isEdited = isEdited;
    this.isDeleted = isDeleted;
    this.userSettings = userSettings;
  }
}

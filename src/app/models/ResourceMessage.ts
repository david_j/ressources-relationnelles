import {User} from './User.model';

export class ResourceMessage {
  id?: string;
  user?: User;
  resource?: string;
  responseToMessage?: string;
  message?: string;
  isResponse?: boolean;
  createdAt?: string;
  editedAt?: string;
  deletedAt?: string;
  isEdited?: boolean;
  isDeleted?: boolean;
  // Not stored in database //
  isFromCurrentUser?: boolean;
}

import {User} from './User.model';

export class RelationRequest {
  _id?: string;
  fromUser?: User;
  toUser?: User;
  status: string;
  responseDate?: string;
  createdAt?: string;
  isDeleted?: boolean;
  deletedAt?: string;

  constructor(_id?: string, fromUser?: User, toUser?: User, status?: string, responseDate?: string, createdAt?: string, isDeleted?: boolean, deletedAt?: string) {
    this._id = _id;
    this.fromUser = fromUser;
    this.toUser = toUser;
    this.status = status;
    this.responseDate = responseDate;
    this.createdAt = createdAt;
    this.isDeleted = isDeleted;
    this.deletedAt = deletedAt;
  }
}
